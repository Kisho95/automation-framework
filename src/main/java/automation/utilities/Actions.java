package automation.utilities;

import automation.config.Drivers;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;


public class Actions {

    WebDriverWait wait;

    public Actions()
    {
        wait = new WebDriverWait(Drivers.driver,10);
    }

    public boolean checkIfElementISDisplayed(WebElement element) {

       try{
            return element.isDisplayed();

        }catch (Exception exc){
            System.out.println(element+" Is not Displayed");
        }
        return false;
    }

    public void clickOn(WebElement element){

        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            if (checkIfElementISDisplayed(element)) {
                element.click();
            }
        }catch (Exception exc){
            System.out.println(element+" is not Displayed");
        }

    }

    public String getTextOf(WebElement element){
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            if(checkIfElementISDisplayed(element)){
                return element.getText();
            }
        }catch (Exception exc){
            System.out.println(element+" is not displayed or Empty");
        }
        return "";
    }
    public boolean CheckIfElementExist(WebElement element){
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            return checkIfElementISDisplayed(element);
        }catch (Exception exc){
            System.out.println(element+" is not Exist");
        }
        return false;

    }

    public boolean CheckIfElementEnabled(WebElement element){
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            if(checkIfElementISDisplayed(element)){
                return element.isEnabled();
            }
        }catch (Exception exc){
            System.out.println(element+" is not Enabled");
        }
        return false;

    }

    public void addDataToField(String data, WebElement element){
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            if(checkIfElementISDisplayed(element)){
                element.clear();
                element.sendKeys(data);
            }
        }catch (Exception exc){
            System.out.println("cannot type On "+element);
        }
    }


    public String getAttribute(WebElement element , String attrName){
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            if(checkIfElementISDisplayed(element)){
                return element.getAttribute(attrName);
            }
        }catch (Exception exc){
            System.out.println(element+ attrName +"is not present");
        }
        return "";
    }

    public void slideVerticallyBetweenTwoElements(MobileElement FromElement , MobileElement ToElement)  {
        try {
            int startx = FromElement.getCenter().x;
            int endx = ToElement.getCenter().x;
            int starty = FromElement.getCenter().y;
            int endy= ToElement.getCenter().y;
            System.out.println("Start(X) Point: "+ startx );
            System.out.println("End(X) Point: "+ endx );
            System.out.println("Start(Y) Point: "+ starty);
            System.out.println("End(Y) Point: "+ endy);

            TouchAction action = new TouchAction(Drivers.driver);
            action.longPress(PointOption.point(startx,starty)).moveTo(PointOption.point(endx, endy)).release().perform();

        } catch (NoSuchElementException exc) {
            System.out.println(FromElement.getClass().getName() +"Element is not found");
        }
    }
}
