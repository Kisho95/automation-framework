package automation.utilities;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import automation.config.Drivers;
import automation.config.FilePaths;
import io.appium.java_client.TouchAction;
import io.appium.java_client.MultiTouchAction;

import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.interactions.touch.TouchActions; // Those are incompatible with Appium and deprecated (were implemented in Chrome only).

import org.openqa.selenium.remote.RemoteWebElement;


public class ScreenFunctions {

	public void getScreenShoot(AppiumDriver<?> driver,String methodName) throws IOException {
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(srcFile, new File("C:\\Users\\ElsayedK2\\Documents\\Khaled\\Udemy\\Selenium\\workspace\\AutomationDemo\\ScreenShoots\\defect.png"));
		System.out.println("ScreenShoot " + System.getProperty("user.dir"));
		FileUtils.copyFile(srcFile, new File(FilePaths.ScreenShoot_filePath +methodName+ System.currentTimeMillis()+".png"));
	}

	public void scrollToText(String text)
	{
		AndroidDriver<AndroidElement>  driver = (AndroidDriver<AndroidElement>) Drivers.driver;
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\""+text+"\"));");
	}

	public void scrollToElementiOS(WebElement element){
		JavascriptExecutor js = (JavascriptExecutor) Drivers.driver;
		Map<String, Object> params = new HashMap<>();
		params.put("direction", "down");
		params.put("element", (element));
		js.executeScript("mobile: swipe", params);
	}
	// Only Works with Chrome
	public void scrollToElement(WebElement element, int xOffset, int yOffset){
		TouchActions action = new TouchActions(Drivers.driver);

		action.scroll(element, xOffset, yOffset);
		action.perform();


	}
	public void scrollDownToCertainWebElement(WebElement element, int timeout, AppiumDriver driver) {
		long TimeMillis = System.currentTimeMillis();
		long startTime = TimeUnit.MILLISECONDS.toSeconds(TimeMillis);
		long endTime = startTime;
		endTime += timeout;
		int retval = 1;

		long currentTime = startTime;
		boolean isDisplayed = false;
		int pressX = 0;
		int topY = 0;
		int bottomY = 0;

		while (endTime >= currentTime) {
			//endTime -= currentTime;
			retval = Long.valueOf(endTime).compareTo(Long.valueOf(currentTime));

			//System.out.println("diferrence :" + (Long.valueOf(endTime)).compareTo(Long.valueOf(currentTime)));
			currentTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

			System.out.println("Current Time :" + currentTime);
			System.out.println("endTime :" + endTime);
			System.out.println("boolean Value of :" + ((boolean)(Long.valueOf(endTime) >= Long.valueOf(currentTime))));
			System.out.println("boolean >= :" + ((boolean)(endTime >= currentTime)));
			System.out.println("retval :" + retval);


			try {
				System.out.println("getSize : " + driver.manage().window().getSize().width);
				System.out.println("getSize : " + driver.manage().window().getSize().height);
				pressX = driver.manage().window().getSize().width / 2;
				bottomY = driver.manage().window().getSize().height;
				topY = driver.manage().window().getSize().height;
                          /*  System.out.println("pressX :"+pressX);
                      System.out.println("bottomY :"+bottomY);
                      System.out.println("topY :"+topY);*/

				isDisplayed = element.isDisplayed();
				if (!isDisplayed) {
					System.out.println("Element Not Displayed in screen functions");
					throw new NoSuchElementException("Element Not Displayed");
				}
				if (isDisplayed) {
					System.out.println("breakkkkkkkkkkkk");
					break;
				}
			} catch (NoSuchElementException e) {
				System.out.println("NoSuchElementException e");
				TouchAction touchAction = new TouchAction(driver);
				touchAction.press(PointOption.point(pressX, bottomY * 4 / 5)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
						.moveTo(PointOption.point(pressX, topY * 40 / 100)).release().perform();
				currentTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
				System.out.println("Current Time2 :" + currentTime);
				System.out.println("endTime2 :" + endTime);
				System.out.println("boolean Value of2 :" + ((boolean)(Long.valueOf(endTime) >= Long.valueOf(currentTime))));
				System.out.println("boolean2 >= :" + ((boolean)(endTime >= currentTime)));
				System.out.println("retval2 :" + retval);
				/*if (currentTime > endTime) {
					throw e;
				}*/
			}
		}
	}
	public void scrollUPToCertainWebElement(WebElement element, int timeout, AppiumDriver driver)
	{
//		try{

		TouchAction action = new TouchAction(driver);
		int pressX = driver.manage().window().getSize().width / 2;
		int topY = driver.manage().window().getSize().height* 1 / 3;
		int bottomY = driver.manage().window().getSize().height* 4 / 5;
		System.out.println("PressX = "+ pressX + " , TopY = "+topY+" , BottomY = " + bottomY);


		action.press(PointOption.point(pressX, topY))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(timeout)))
				.moveTo(PointOption.point(pressX, bottomY))
				.release()
				.perform();

//		}catch{
//		}
	}
}



