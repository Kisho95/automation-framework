package automation.config;

public class FilePaths {

	public final static String AndroidDesiredCap_filePath = System.getProperty("user.dir")+"\\Taregets\\AndroidDriver.properties";
	public final static String objectIDs_folderPath = System.getProperty("user.dir")+"\\ObjectIDs\\";
	public final static String objectIDs_filePath = System.getProperty("user.dir")+"\\objectIDs.xlsx";
	public final static String appURL = "https://assets-es-staging2.myvdf.aws.cps.vodafone.com/mves/login";
	public final static String ScreenShoot_filePath = System.getProperty("user.dir")+"\\ScreenShoots\\";
	public final static String Reports_filePath = System.getProperty("user.dir")+"\\Reports\\";
	public final static String deepLinksURL = "https://meladraouf.github.io/deeplinks/DeepLinks.html";
	public final static String WebBuySIM = "https://assets-es-pprd.dxlpreprod.local.vodafone.es/mves/buySim/personelInfo?tariffName=tarifa-prepago-s";
}



