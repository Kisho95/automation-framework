package automation.config;

import automation.filesHandeler.TextFile;
import automation.pom.*;
import automation.utilities.ScreenFunctions;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Drivers{
	public static RemoteWebDriver remoteDriver;
	public static AppiumDriver<?> driver;
	public static IOSDriver<IOSElement> iosDriver;
	TextFile textFile = new TextFile();
	Properties prop = new Properties();
	public void initDriver(String target) throws IOException {

		System.out.println("target is " + target);
		System.out.println("target in Driver " + target);


		// TODO Auto-generated method stub

		switch (target) {
			case "Android":
				androidDriver();
				break;
			
		}

		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Initializations inti = new Initializations();
		//inti.startServer();
	}
	public void androidDriver() throws IOException {

		FileInputStream fis = new FileInputStream(FilePaths.AndroidDesiredCap_filePath);
		prop.load(fis);



		Cap androidCap  = new Cap((String)prop.get("deviceName"),
				
				(String)prop.get("platformName"),
				(String)prop.get("platformVersion"),
				(String)prop.get("appPackage"),
				(String)prop.get("appActivity"),
				Boolean.parseBoolean((String)prop.get("skipUnlock")),
				Boolean.parseBoolean((String)prop.get("noReset")),
				(String)prop.get("server"));





		DesiredCapabilities desiredCaps = new DesiredCapabilities();
		desiredCaps.setCapability("deviceName", androidCap.deviceName);
		//desiredCaps.setCapability("udid", androidCap.udid); // DeviceId from "adb devices" command
		desiredCaps.setCapability("platformName", androidCap.platformName);
		desiredCaps.setCapability("platformVersion", androidCap.platformVersion);
		desiredCaps.setCapability("skipUnlock", androidCap.skipUnlock);
		desiredCaps.setCapability("appPackage", androidCap.appPackage);
		desiredCaps.setCapability("appActivity", androidCap.appActivity);
		desiredCaps.setCapability("noReset", androidCap.noReset);
		desiredCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		//desiredCaps.setCapability("newCommandTimeout", 300);
		//desiredCaps.setCapability("skipServerInstallation", true);
		//desiredCaps.setCapability("adbExecTimeout", 3000);

		//desiredCaps.setCapability(MobileCapabilityType);

		driver = new AndroidDriver<AndroidElement>(
				new URL(androidCap.server), desiredCaps);
		//return driver;
	}








}
