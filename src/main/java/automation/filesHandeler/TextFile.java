package automation.filesHandeler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TextFile {
	
	
	
	public String[] readTextFile(String filePath) {
		
		
	    String[] splittedString=null;

		
		try(BufferedReader in = new BufferedReader(new FileReader(filePath))) {
			
		    String str;
		    String wholeText="";
 
		    while ((str = in.readLine()) != null) {
		    	//wholeText = wholeText.append(str);
		        wholeText = wholeText.concat(str);
		    	//System.out.println(str);
		    }
		   // System.out.println("the whole text is: "+ wholeText);
		    splittedString = wholeText.split(",");
		    //System.out.println("The first cell is: "+splittedString[0]);
		    
		    
		}
		catch (IOException e) {
		    System.out.println("File Read Error");
		}
		return splittedString;
	}
}
