package automation.modules;

import automation.config.Drivers;
import automation.pom.RHBPage;
import automation.utilities.ScreenFunctions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;

public class RHBModule extends Drivers {


    ScreenFunctions screenFunctions;
 
    WebDriverWait wait;
    SoftAssert softAssert;
   
    RHBPage rhbpage;




    @Parameters({ "Target"})
    @BeforeTest(alwaysRun = true)
    public void setUp(String target) throws Exception {
        initDriver(target);
        wait = new WebDriverWait(Drivers.driver, 30);
        
        screenFunctions = new ScreenFunctions();
       
        rhbpage = new RHBPage(Drivers.driver);
        System.out.println("target is " + target);
        softAssert =  new SoftAssert();
     //   loginPage.generalLogin(target,username,password);

        // TODO Auto-generated method stub
    }


/*-----------------------------Mis Datos Test Cases-------------------------*/
    @Test(priority = 1)
    public void clickonENG() {

    	
    	rhbpage.clickOnRHB_ENG();
       

    }
 
    @Test(priority = 2)
    public void clickonLoin() {

    	
    	rhbpage.clickOnRHB_Login();      

    }

}
